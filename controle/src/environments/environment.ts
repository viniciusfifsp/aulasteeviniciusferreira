// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAwLkqJMJG-JI-GLn6A_got48bBLKaCvCg',
    authDomain: 'controle-tee.firebaseapp.com',
    databaseURL: 'https://controle-tee.firebaseio.com',
    projectId: 'controle-tee',
    storageBucket: 'controle-tee.appspot.com',
    messagingSenderId: '464005668719',
    appId: '1:464005668719:web:6b59cde5bf66315725929a',
    measurementId: 'G-4M674YCG49'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
